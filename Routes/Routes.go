package Routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/barry_nevio/api-framework/instances/GetWidget"
)

type Route struct {
	Method  string
	Path    string
	Handler func(c *gin.Context)
}

func Get() []Route {
	var routes []Route

	getWidget := Route{}
	getWidget.Method = "GET"
	getWidget.Path = "widget"
	getWidget.Handler = func(c *gin.Context) {
		instance := GetWidget.New(c)
		instance.Run()
		c.JSON(instance.View.HTTPStatusCode, instance.View.Map)
		return
	}

	routes = append(routes, getWidget)

	return routes
}
