package GetWidget

import (
	"encoding/json"
)

// WidgetView ...
type WidgetView struct {
	ID        int64  `json:"id"`
	UUID      string `json:"uuid"`
	AddedAt   string `json:"added_at"`
	UpdatedAt string `json:"updated_at"`
}

// View ...
type View struct {
	Widgets []WidgetView `json:"widgets"`
}

// GetAsMap ...
func (v *View) GetAsMap() map[string]interface{} {
	mapView := make(map[string]interface{})
	byteView, _ := json.Marshal(v)
	json.Unmarshal(byteView, &mapView)
	return mapView
}
