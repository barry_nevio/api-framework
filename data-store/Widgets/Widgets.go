package Widgets

import (
	"database/sql"

	"github.com/spf13/cast"
	"gitlab.com/barry_nevio/api-framework/models/Widget"
	"gitlab.com/barry_nevio/goo-tools/Database"
)

// Table ...
type Table struct {
	DBConn *Database.Connection
	Name   string
}

// New ...
func New(dbConn *Database.Connection) Table {
	t := Table{}
	t.DBConn = dbConn
	t.Name = "widgets"
	return t
}

// Add ..
func (t *Table) Add(m Widget.Model) (Widget.Model, error) {

	return m, nil
}

// Get ...
func (t *Table) Get(limit, offset int) ([]Widget.Model, error) {
	var widgets []Widget.Model
	var err error

	q := `SELECT ` +
		`widgets.id, ` +
		`widgets.uuid, ` +
		`widgets.added_at, ` +
		`widgets.updated_at, ` +
		`FROM widgets LEFT JOIN widget_families ` +
		`ON widgets.id = widget_families.widget_id ` +
		`WHERE widgets.active = 1 ` // WE ONLY WANT ACTIVE WIDGETS

	// Add limits and offset
	if limit > 0 {
		q = q + `LIMIT ` + cast.ToString(limit) + ` OFFSET ` + cast.ToString(offset) + ` `
	}

	db, err := t.DBConn.Get()
	if err != nil {
		return nil, err
	}

	rows, err := db.Query(q)
	if err != nil {
		return widgets, err
	}

	return t.ProcessSelectQuery(rows)
}

// ProcessSelectQuery ...
func (t *Table) ProcessSelectQuery(rows *sql.Rows) ([]Widget.Model, error) {
	var widgets []Widget.Model

	for rows.Next() {

		// Use a rough
		widgetRough := Widget.Rough{}

		err := rows.Scan(
			&widgetRough.ID,
			&widgetRough.UUID,
			&widgetRough.AddedAt,
			&widgetRough.UpdatedAt,
		)
		if err != nil {
			rows.Close()
			return widgets, err
		}

		// Append refined widget
		widgets = append(widgets, widgetRough.Refine())

	}

	rows.Close()
	return widgets, nil
}
