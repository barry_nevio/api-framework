package main

import (
	"log"

	"github.com/apex/gateway"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"gitlab.com/barry_nevio/api-framework/Routes"
	"gitlab.com/barry_nevio/api-framework/models/Settings"
)

func main() {

	settings := Settings.New()
	_ = settings.SetFromJSONFile()

	// Set server mode
	if settings.HTTPServer.ReleaseMode {
		gin.SetMode(gin.ReleaseMode)
	} else {
		gin.SetMode(gin.DebugMode)
	}

	// Instanciate default gin router
	r := gin.Default()

	// Get all routes
	routes := Routes.Get()

	// Add routes to router
	for _, route := range routes {
		switch route.Method {
		case "GET":
			r.GET(route.Path, route.Handler)
		case "POST":
			r.POST(route.Path, route.Handler)
		case "PUT":
			r.PUT(route.Path, route.Handler)
		case "DELETE":
			r.DELETE(route.Path, route.Handler)
		case "ANY":
			r.Any(route.Path, route.Handler)
		case "OPTIONS":
			r.OPTIONS(route.Path, route.Handler)
		case "PATCH":
			r.PATCH(route.Path, route.Handler)
		case "HEAD":
			r.HEAD(route.Path, route.Handler)
		}
	}

	// AWS API Gateway
	if settings.HTTPServer.AWSAPIGateway {
		log.Fatal(gateway.ListenAndServe(":80", r))
	}
	if !settings.HTTPServer.AWSAPIGateway {
		addr := ":" + cast.ToString(settings.HTTPServer.Port)
		r.Run(addr)
	}

}
