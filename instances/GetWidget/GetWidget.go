package GetWidget

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"gitlab.com/barry_nevio/api-framework/data-store/Widgets"
	"gitlab.com/barry_nevio/api-framework/models/Error"
	"gitlab.com/barry_nevio/api-framework/models/Settings"
	"gitlab.com/barry_nevio/api-framework/models/View"
	"gitlab.com/barry_nevio/api-framework/models/Widget"
	"gitlab.com/barry_nevio/api-framework/views/GetWidget"
	"gitlab.com/barry_nevio/goo-tools/Database"
)

type Instance struct {
	Request struct {
		Headers http.Header
		Body    []byte
	}
	Settings   Settings.Model
	DBConn     Database.Connection
	Widgets    []Widget.Model
	WidgetUUID string
	Limit      string
	Offset     string
	View       View.Model
	State      struct {
		Error Error.Model
	}
}

func New(ginCtx *gin.Context) Instance {
	i := Instance{}
	i.Request.Body, _ = ginCtx.GetRawData()
	i.Request.Headers = ginCtx.Request.Header
	i.WidgetUUID = strings.TrimSpace(ginCtx.Param("widget_uuid"))
	i.Limit = strings.TrimSpace(ginCtx.Param("limit"))
	i.Offset = strings.TrimSpace(ginCtx.Param("offset"))
	i.Settings = Settings.New()
	// ! Important, potential error createdhere,
	// ! make sure you check the instance state after instanciation
	i.DBConn, i.State.Error.Err = Database.New(i.Settings.Database)
	if i.State.Error.Err != nil {
		i.State.Error.Application = true
		return i
	}
	return i
}

func (i *Instance) Run() {
	dataStore := Widgets.New(&i.DBConn)
	i.Widgets, i.State.Error.Err = dataStore.Get(cast.ToInt(i.Limit), cast.ToInt(i.Offset))

	v := GetWidget.View{}

	for _, widget := range i.Widgets {
		wv := GetWidget.WidgetView{}
		wv.UUID = widget.UUID
		v.Widgets = append(v.Widgets, wv)
	}

	i.View.HTTPStatusCode = 200
	i.View.Map = v.GetAsMap()
}
