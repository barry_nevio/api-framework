package Widget

import (
	"github.com/spf13/cast"
)

// Model ...
type Model struct {
	ID        int64  `json:"id"`
	UUID      string `json:"uuid"`
	AddedAt   string `json:"added_at"`
	UpdatedAt string `json:"updated_at"`
}

// Rough ...
type Rough struct {
	ID        interface{} `json:"id"`
	UUID      interface{} `json:"uuid"`
	AddedAt   interface{} `json:"added_at"`
	UpdatedAt interface{} `json:"updated_at"`
}

// Refine ...
func (r *Rough) Refine() Model {
	m := Model{}
	m.ID = cast.ToInt64(r.ID)
	m.UUID = cast.ToString(r.UUID)
	m.AddedAt = cast.ToString(r.AddedAt)
	m.UpdatedAt = cast.ToString(r.UpdatedAt)
	return m
}
