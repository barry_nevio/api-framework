package Settings

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"gitlab.com/barry_nevio/goo-tools/Database"
)

type HTTPServer struct {
	AWSAPIGateway bool `json:"aws_api_gateway"`
	ReleaseMode   bool `json:"release_mode"`
	Port          int  `json:"port"`
}

type Model struct {
	Database   Database.Settings `json:"database"`
	HTTPServer HTTPServer        `json:"http_server"`
}

func New() Model {
	m := Model{}

	return m
}

func (m *Model) SetFromJSONFile() error {
	// Open Settings File
	settingsReader, openSettingsErr := os.Open("./settings.json")
	if openSettingsErr != nil {
		return openSettingsErr
	}
	defer settingsReader.Close()

	// Read settings file
	settingsBytes, settingsReadErr := ioutil.ReadAll(settingsReader)
	if settingsReadErr != nil {
		return settingsReadErr
	}

	// Marshal
	_ = json.Unmarshal(settingsBytes, &m)

	// Close
	settingsReader.Close()

	return nil
}
