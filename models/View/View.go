package View

type Model struct {
	HTTPStatusCode int
	Map            map[string]interface{}
	String         string
}
