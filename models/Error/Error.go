package Error

type Model struct {
	Err          error `json:"-"`
	Validation   bool  `json:"validation_error"`  // 422
	Application  bool  `json:"application_error"` // 500
	Forbidden    bool  `json:"forbidden"`         // 403
	TokenExpired bool  `json:"token_expired"`     // 401
	NotFound     bool  `json:"not_found"`         // 404
}
